from django.urls import path
from .views import ListArticle, DetailArticle, ListUsers,DetailUsers

urlpatterns = [
    path("", ListArticle.as_view()),
    path("<int:pk>/",DetailArticle .as_view()),
    path("user_api/",ListUsers.as_view()),
    path("user_api/<int:pk>/",DetailUsers.as_view()),
   
]