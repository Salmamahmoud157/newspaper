from django.contrib import admin

from .models import Article, Comment

class CommentInLine(admin.TabularInline):
    model = Comment

class ArticaleAdmin(admin.ModelAdmin):
    inlines = [
        CommentInLine,
    ]

# Register your models here.
admin.site.register(Article, ArticaleAdmin)
admin.site.register(Comment)