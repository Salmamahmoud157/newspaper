import React, {
  Component
} from 'react';
import axios from 'axios';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  state = {
    articles: []
  };

  componentDidMount() {
    this.getArticles();
  }
  getArticles() {
    axios
      .get('http://127.0.0.1:8000/api/')
      .then(res => {
        this.setState({
          articles: res.data
        });
      })
      .catch(err => {
        console.log(err);
      });
  }
  render() {
    return ( 
      <div>
        {this.state.articles.map(item => (
            <div key={item.id}>
              <h1>{item.title}</h1>
              <span>{item.body}</span>
            </div>
        ))}
      </div>
    );
  }
}
export default App;